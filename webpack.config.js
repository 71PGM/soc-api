const HtmlWebPackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const BundleTracker = require('webpack-bundle-tracker');

let pathsToClean = ['dist', 'build'];
let cleanOptions = {root: '/full/webpack/root/path', exclude: ['shared.js'], verbose: true, dry: false};

module.exports = options => {
    return {
        entry: './assets/js/index.js',
        output: {
            path: __dirname + '/assets/public/',
            publicPath: '/',
            filename: 'static/bundle.js'
        },
        resolve: {
            extensions: ['.js', '.jsx', '.css', '.js', '.vue']
        },
        module: {
            rules: [
                {
                    test: /\.html$/,
                    use: [
                        {
                            loader: "html-loader",
                            options: { minimize: true }
                        }
                    ]
                },
                {
                   test: /\.vue$/,
                   loader: 'vue-loader'
                 }, {
                      test: /\.css$/,
                      use: [
                            'vue-style-loader',
                            'css-loader'
                          ]
                }
            ]
        },
        plugins: [
            new CleanWebpackPlugin(pathsToClean, cleanOptions),
            new HtmlWebPackPlugin(),
            new VueLoaderPlugin(),
            new BundleTracker( {filename: './webpack-stats.json'}),
        ]
    }
};
