window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');
import Vue from 'vue';

import SocApi from "./components/SocApi.vue";

const app = new Vue({
    el: '#app',
    components: {
        SocApi
    }
});